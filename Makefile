SYSTEMD_UNITS_DIR = /usr/lib/systemd
.PHONY: install

#config.template  docker@.service  systemd-docker.service  systemd-docker@.service
install:
	install -d $(INSTALL_ROOT)/$(SYSTEMD_UNITS_DIR)/system
	install -m 0644 docker@.service $(INSTALL_ROOT)/$(SYSTEMD_UNITS_DIR)/system/
	install -m 0644 systemd-docker.service $(INSTALL_ROOT)/$(SYSTEMD_UNITS_DIR)/system/
	install -m 0644 systemd-docker@.service $(INSTALL_ROOT)/$(SYSTEMD_UNITS_DIR)/system/

        install -d $(INSTALL_ROOT)/etc/docker-configs/
        touch $(INSTALL_ROOT)/etc/docker-configs/.dummy
