# Container units

This allows containers to be managed with systemd by 
using generator units.  This also supports running
docker containers that use systemd to manage services
internally.  Dockers are started by their image
name so they need to be escaped to follow systemd
rules
[systemd-escape(1)](http://www.freedesktop.org/software/systemd/man/systemd-escape.html).
So if the image is named ```user/app``` the unit 
would be escaped as ```user-app```.

To run a docker that uses supervisor, or a script
use ```systemctl start docker@image-name.service```
Likewise the docker can be set to be run at system
startup using ```systemctl enable docker@image-name.service```
For dockers that use systemd use 
```systemctl start systemd-docker@image-name.service```
so that the cgroups are moved to the correct scopes.

**NOTE:** For larger containers it is best to "pull"
them first since the unit will time out after 
240 seconds.

# Special Configurations

For dockers that require extra options such
as adding environment variables, exposing
ports or linking containers the unit will
look for a configuration file ``/etc/docker-configs/%i.conf``
where ``%i`` would the the escaped container
name. If this file exsists and has the
shell variable ``DOCKER_OPTS`` defined
with ``docker run`` arguments they will be 
passed to the unit.  By default these
units will log to the journal so if
this is not desired add ``--log-driver=none``
to the ``DOCKER_OPTS``.

**NOTE:** The docker run arguments ``--name``
and ``--rm`` are already provided to ensure
correct operation of the unit. The
option ``-d`` must not be used because the
service handles the lifetime of the container.

# Install

```
$ sudo make install
```

# TODO

1. Figure out how to support tags
1. Maybe define the unit by name and use the 
   config file to select the image instead.

